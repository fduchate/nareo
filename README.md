[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3904419.svg)](https://doi.org/10.5281/zenodo.3904419)

# NAREO Ontology

We present an ontological model NAREO (Neighbourhood And Real Estate Ontology) that integrates data related to neighborhood recommendation. 
This representation considers spatial data about the environment forming a neighborhood (amenity, services,leisure, shops). 
We reuse the GeoSPARQL standard ontology that eases the spatial representation in NAREO. 
The taxonomy presented gathers the main concepts related to a neighborhood and relations to link semantically the data stored.

# Acknowledgement

This work has been partially funded by LABEX IMU (ANR-10-LABX-0088) from Université de Lyon, in the context of the program "Investissements d'Avenir" (ANR-11-IDEX-0007) from the French Research Agency (ANR), during the [HiL project](http://imu.universite-lyon.fr/projet/hil/)